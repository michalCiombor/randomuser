import React from 'react';
import './Person.css';

class Person extends React.Component {
    state = {
        name: false,
        surname: false,
        gender: false

    }
    show = (id) => {
        if (id === "name") {
            this.setState({
                name: true,
                surname: false,
                gender: false
            })

        }
        else if (id === "surname") {
            this.setState({
                name: false,
                surname: true,
                gender: false
            })

        }
        else {
            this.setState({
                name: false,
                surname: false,
                gender: true
            })
        }
    }
    render() {
        return (
            <div className="showPerson">
                <div className="img">
                    <img src={this.props.picture} alt="" />
                </div>
                <div className="buttony">
                    <button onClick={() => this.show("name")}>imie</button>
                    <button onClick={() => this.show("surname")}>nazwisko</button>
                    <button onClick={() => this.show("gender")}>płeć</button>
                </div>
                <div id="podpisy">
                    {this.state.name ? this.props.name : ""}
                    {this.state.surname ? this.props.surname : ""}
                    {this.state.gender ? this.props.gender : ""}
                </div>





            </div >
        )
    }
}
export default Person;