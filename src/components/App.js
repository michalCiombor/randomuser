import React from 'react';
import './App.css';
import Button from './Button';
import Person from './Person';

class App extends React.Component {
  state = {
    user: [],
    clicked: false,
  }

  // componentDidMount() {
  //   this.getData();
  // }


  getData = () => {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", "https://randomuser.me/api/?results=4", true);
    xhr.send(null);
    xhr.onload = () => {

      const data = JSON.parse(xhr.response);
      this.setState({
        user: data.results,
        clicked: true,

      })
      // console.log(this.state.user)
    }
  }
  handleButtonClick = () => {
    this.setState({
      clicked: true,
    })
  }

  render() {
    const data = this.state.user.map(el => <div className="list" key={el.login.uuid}> <Person picture={el.picture.large} name={el.name.first} surname={el.name.last} gender={el.gender} /></div>);

    return (

      < div >
        <Button onClick={this.getData} />

        {this.state.clicked ? data : ""}

      </div>
    )
  }
}

export default App;